import React, { Component } from 'react';

// this.props = {
//  loaded: bool,
//  links: [ { name: '', filename: '' }, ... ]
// };

class Category extends Component {
  constructor(props) {
    super(props);
    this.state = {
      linkIndex: -1,
      error: null,
      pageLoaded: false,
      page: null
    };

    this.setLink = this.setLink.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.loaded !== this.props.loaded) {
      this.setState({
        linkIndex: -1,
        page: null,
        pageLoaded: false
      });
    }
  }

  fetchData(filename) {
    fetch(filename)
      .then(res => res.text())
      .then(
        (result) => {
          console.log(result);
          this.setState({
            pageLoaded: true,
            page: result,
            error: null
          });
        },
        (error) => {
          console.log(error);
          this.setState({
            pageLoaded: false,
            error
          });
        }
      )
  }

  setLink(e) {
    let index = parseInt(e.target.value);
    this.setState({
      linkIndex: index,
      pageLoaded: false,
      page: null
    });
    if (index >= 0) {
    let filename = this.props.links[index].filename;
    console.log(this.props.links[index]);
    console.log(filename);

    this.fetchData(filename);
    }
  }

  render() {
    const myPage = this.state.page;

    if (!this.props.loaded) {
      return <div>Loading ...</div>
    } else {
      return (
        <div>
          <select onChange={this.setLink} value={this.state.linkIndex}>
            <option key={-1} value={-1}>---</option>
          {this.props.links.map((item, i) => (
            <option key={i} value={i}>{item.name}</option>
          ))}
          </select>


        <div dangerouslySetInnerHTML={{__html: myPage}}></div>
        </div>
      );
    }
  }
}

export default Category;
