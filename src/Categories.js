import React, { Component } from 'react';
import Error from './Error.js';
import Category from './Category.js';

// this.props = {
//  loaded: bool,
//  categories: [ { name: '', filename: '' }, ... ]
// };

class Categories extends Component {
  constructor(props) {
    super(props);

    this.state = {
      categoryIndex: -1,
      linkLoaded: false,
      links: [],
      error: null
    }

    this.setCategory = this.setCategory.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.loaded !== this.props.loaded) {
      /* sono arrivare le categorie */
      this.setState({
        categoryIndex: 0
      });
      this.fetchLinks(this.props.categories[0].filename);
    }
  }

  fetchLinks(filename) {
    fetch(filename)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            linkLoaded: true,
            links: result.items,
            error: null
          });
        },
        (error) => {
          this.setState({
            error
          });
        }
      )
  }

  setCategory (e) {
    var newCategoryIndex = parseInt(e.target.value);
    this.setState({
      categoryIndex: newCategoryIndex,
      linkLoaded: false,
      links: []
    });
    this.fetchLinks(this.props.categories[newCategoryIndex].filename);
  }
  
  render() {
    if (!this.props.loaded) {
      return <div>Loading ...</div>
    } else {
      return (
        <div>
        <select onChange={this.setCategory} value={this.state.categoryIndex}>
          {this.props.categories.map((item, i) => (
            <option key={i} value={i}>{item.name}</option>
          ))}
        </select>

        <Error error={this.state.error} />
        <Category loaded={this.state.linkLoaded}
                  links={this.state.links} />
        </div>
      );
    }
  }
}

export default Categories;
