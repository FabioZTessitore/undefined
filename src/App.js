import React, { Component } from 'react';
import Error from './Error.js';
import Categories from './Categories.js';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      categories: [],
      loaded: false,
      error: null
    }

  }

  fetchCategories() {
    fetch("categories.json")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            categories: result.items,
            loaded: true,
            error: null
          });
        },
        (error) => {
          this.setState({
            error
          });
        }
      )
  }

  componentDidMount() {
    this.fetchCategories();
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1>Undefined</h1>
          <Error error={this.state.error} />
          <Categories loaded={this.state.loaded}
                      categories={this.state.categories} />
        </header>
      </div>
    );
  }
}

export default App;
