import React, { Component } from 'react';

class Error extends Component {

  render() {
    if (this.props.error) {
      return <div>Error: {this.props.error.message}</div>
    }

    return null;
  }
}

export default Error;
